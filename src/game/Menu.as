package game
{
	import flash.display.BitmapData;
	
	import game.entity.BaseEntity;
	import game.entity.Player;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.World;
	import net.flashpunk.graphics.Canvas;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.tweens.misc.Alarm;
	import net.flashpunk.tweens.misc.VarTween;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	import punk.fx.effects.GlitchFX;
	import punk.fx.effects.RGBDisplacementFX;
	import punk.fx.graphics.FXImage;
	import punk.fx.graphics.FXLayer;
	
	import sprout.flashpunk.FXWord;
	import sprout.graphics.BitmapDataHelper;
	import sprout.graphics.MyColors;
	
	public class Menu extends FXWord
	{
		private var _level1Button: Entity;
		private var _level2Button: Entity;
		private var _level3Button: Entity;
		
		private var _howToPlay: Text;
		
		private var _player: Player;
		private var _lighting: Lighting;
		
		public function Menu()
		{
			super();
		}
		
		override public function begin():void {
			super.begin();
			
			Lighting.lighteningEffectRatio = 0;
			add(_lighting = new Lighting());
			// #812380
			var bitmapData: BitmapData = new BitmapData(FP.width, FP.height, false);
			BitmapDataHelper.drawGradient(bitmapData, 0xE140FE, 0x812380);
			var canvas: Canvas = new Canvas(FP.width, FP.height);
			canvas.draw(0, 0, bitmapData);
			addGraphic(canvas);
			/*
			var background: Entity = new Entity();
			background.graphic = canvas;
			background.visible = false;
			add(background);
			*/
			FP.screen.color = 0xE140FE;//MyColors.PINK;
			
			var title: Text = new Text(
				"PESTS", 0, 32, 
				{
					size: 64, 
					color: MyColors.PINK, 
					width: FP.width, 
					wordWrap: true, 
					align: "center"
				} 
			);
			//addGraphic(title);
			
			_howToPlay = new Text(
				"Collect as many pills as possible", 0, 90, 
				{
					size: 8, 
					color: MyColors.PINK, 
					width: FP.width, 
					wordWrap: true, 
					align: "center"
				} 
			);
			addGraphic(_howToPlay);
			
			_addPlayer(false);
			
			add(_level1Button = new Entity(11 * Level.TILE_SIZE + Level.TILE_SIZE / 2, 8 * Level.TILE_SIZE, new Stamp(Assets.Pill)));
			_level1Button.setHitboxTo(_level1Button.graphic);
			_level1Button.type = "button1";
			
			var rgbDisplacement: RGBDisplacementFX = new RGBDisplacementFX([3, 2, 1, 3, 2, 1]);
			var glitch: GlitchFX = new GlitchFX(5, 6);
			
			var tileEntity: Entity = new Entity();
			tileEntity.graphic = title;
			tileEntity.visible = false;
			
			var fxLayer: FXLayer = new FXLayer();  // with no parameters will default to the size of the screen
			fxLayer.entities.add(tileEntity);
			fxLayer.effects.add([glitch, rgbDisplacement]);
			addGraphic(fxLayer, 0);
			
			add(tileEntity);
			
			
			
			
			
			/*
			add(_level2Button = new Entity(12 * Level.TILE_SIZE, 8 * Level.TILE_SIZE, new Stamp(Assets.Pill)));
			_level2Button.type = "button2";
			//(_level2Button.graphic as Image).color = MyColors.GRAY;
			
			add(_level3Button = new Entity(15 * Level.TILE_SIZE, 8 * Level.TILE_SIZE, new Stamp(Assets.Pill)));
			_level3Button.type = "button3";
			//(_level2Button.graphic as Image).color = MyColors.GRAY;
			*/
			
			var aMusicBy: Text = new Text(
				"With a music by\n @BobbyDrano", 0, 176, 
				{
					//font: "KBerryFont",
					//font: "BlockedFont",
					//size: 14, 
					color: MyColors.BLACK, 
					width: FP.width, 
					wordWrap: true, 
					align: "center"
				} 
			);
			addGraphic(aMusicBy);
			
			var thanks: Text = new Text(
				"And a special thanks to \nZachary and Saturnyn.", 0, 210, 
				{
					//font: "KBerryFont",
					//font: "BlockedFont",
					size: 8, 
					color: MyColors.BLACK, 
					width: FP.width, 
					wordWrap: true, 
					align: "center"
				} 
			);
			addGraphic(thanks);
		}
		
		private function _addPlayer(fade: Boolean = true): void {
			_player = new Player();
			_player.reset(9 * Level.TILE_SIZE + Level.TILE_SIZE / 2, 8 * Level.TILE_SIZE - Level.TILE_SIZE / 2);
			if (fade) {
				var sprite: Image = (_player.graphic as Image);
				sprite.alpha = 0;
				var varTween: VarTween = new VarTween();
				varTween.tween(sprite, "alpha", 1, 0.6);
				addTween(varTween, true);
			}
			add(_player);
			_lighting.addLight(_player.light);
		}
		
		override public function update():void {
			super.update();
			/*
			if (Input.pressed(Key.SPACE)) {
				FP.world = new Level(1);
			}
			*/
			if (_player == null) return;
			if (_player.dead == true) {
				_player = null;
				addTween(new Alarm(1, _addPlayer), true);
				return;
			}
			
			if (_player.moving) {
				_howToPlay.text = "Catch the pills";
			} else {
				_howToPlay.text = "ASDW: Move - ARROWS: Attack - E: Bomb";
			}
			
			if (_player.x < 0) {
				_player.x = 0;
			} else if (_player.x + _player.width > FP.width) {
				_player.x = FP.width - _player.width;
			}
			if (_player.y < 0) {
				_player.y = 0;
			} else if (_player.y + _player.height > FP.height) {
				_player.y = FP.height - _player.height;
			}
			
			if(_player.collide("button1", _player.x, _player.y)) {
				FP.world = new Level(1, _player.x, _player.y);
			}
			
			if(_player.collide("button2", _player.x, _player.y)) {
				FP.world = new Level(2);
			}
		}
	}
}