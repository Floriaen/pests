package game
{
	public class Assets
	{
		// FONTS:
		[Embed(source = '/resources/fonts/origa___.ttf', embedAsCFF="false", fontFamily = 'KBerryFont')]
		public static const KBerry:Class;
		
		[Embed(source = '/resources/fonts/blocked.ttf', embedAsCFF="false", fontFamily = 'BlockedFont')]
		public static const Blocked:Class;
		
		[Embed(source = '/resources/fonts/metropolische.ttf', embedAsCFF="false", fontFamily = 'MetroFont')]
		public static const Metro:Class;
		
		// LEVELS:
		
		// ANTIQ
		[Embed(source="/resources/levels/level1.tmx", mimeType="application/octet-stream")]
		public static const Level1: Class;
		[Embed(source="/resources/graphics/map1.png")]
		public static const Map1: Class;
		
		// GLOOM
		[Embed(source="/resources/levels/level2.tmx", mimeType="application/octet-stream")]
		public static const Level2: Class;
		[Embed(source="/resources/graphics/map2.png")]
		public static const Map2: Class;
		
		// SOUNDS
		
		[Embed(source="/resources/sounds/gunfire.mp3")]
		public static const GunFireFX: Class;
		[Embed(source="/resources/sounds/explosion.mp3")]
		public static const ExplosionFX: Class;
		[Embed(source="/resources/sounds/sonar.mp3")]
		public static const HornFX: Class;
		[Embed(source="/resources/sounds/pests.mp3")]
		public static const MainTheme: Class;
		
		
		
		[Embed(source="/resources/graphics/dust.png")]
		public static const Dust: Class;
		
		[Embed(source="/resources/graphics/energy.png")]
		public static const Energy: Class;
		
		[Embed(source="/resources/graphics/player.png")]
		public static const Player: Class;
		
		[Embed(source="/resources/graphics/small.png")]
		public static const Small: Class;
		
		[Embed(source="/resources/graphics/big.png")]
		public static const Big: Class;
		
		[Embed(source="/resources/graphics/bumber.png")]
		public static const Bumper: Class;
		
		[Embed(source="/resources/graphics/bullet.png")]
		public static const Bullet: Class;
		
		[Embed(source="/resources/graphics/portal.png")]
		public static const Portal: Class;
		
		[Embed(source="/resources/graphics/bomb.png")]
		public static const Bomb: Class;
		
		[Embed(source="/resources/graphics/pipe.png")]
		public static const Pipe: Class;
		
		[Embed(source="/resources/graphics/pill.png")]
		public static const Pill: Class;
		
		[Embed(source="/resources/graphics/smallExplosion.png")]
		public static const SmallExplosion: Class;
		
		[Embed(source="/resources/graphics/explosion.png")]
		public static const Explosion: Class;
		
		[Embed(source="/resources/graphics/smoke.png")]
		public static const Smoke: Class;
		
		[Embed(source="/resources/graphics/pause.png")]
		public static const Pause: Class;
		
		[Embed(source="/resources/graphics/light.png")]
		public static const Light: Class;
		
		[Embed(source="/resources/graphics/fire.png")]
		public static const Fire: Class;
		
		
	}
}