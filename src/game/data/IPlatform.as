package game.data
{
	public interface IPlatform
	{
		function get score(): int;
		function set score(value: int): void;
		function get username(): String;
		function unlockAchievement(name: *): void;
	}
}