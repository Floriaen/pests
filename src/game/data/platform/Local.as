package game.data.platform
{
	import game.data.IPlatform;
	
	import net.flashpunk.utils.Data;
	
	public class Local implements IPlatform
	{
		protected var game: String;
		protected var version: Number;
		private var _fileStorage: String;
		
		public function Local(game: String, version: Number = 1) {
			game = game;
			version = version;
			_fileStorage = game;
		}
		
		public function set score(value: int):void {
			Data.writeInt("score", value);
			Data.save(_fileStorage);
		}
		
		public function get score():int {
			Data.load(_fileStorage);
			return Data.readInt("score", 0);
		}
		
		public function get username():String {
			return null;
		}
		
		public function unlockAchievement(name: String): void {
			// no op
		}
	}
}