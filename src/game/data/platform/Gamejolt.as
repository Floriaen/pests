package game.data.platform
{
	import com.gamejolt.API;
	
	import game.data.IPlatform;
	
	import net.flashpunk.FP;
	
	import sprout.core.RestHelper;
	
	public class Gamejolt implements IPlatform
	{		
		private static const GAME_ID: int = 15052;
		private static const PRIVATE_KEY: String = "ae869d58b8d92b2df496ba8960c3bafa";
		
		public static var unlocked: Object = new Object();
		public static const DUMMY_TROPHY: int = 2276;
		public static const ASSASIN: int = 2277;
		public static const GENOCIDE: int = 2278;
		
		private var _api: API;
		private var _username: String;
		private var _token: String;
		
		private var _score: int = 0;
		private var _connected: Boolean = false;
		
		public function Gamejolt()
		{
			FP.console.log("Gamejolt");
			var paramList:Object = FP.stage.root.loaderInfo.parameters;
			// gjapi_token
			if (paramList && paramList.gjapi_username) {
				this._username = paramList.gjapi_username;
				this._token = paramList.gjapi_token;
			
				_api = new API();
				FP.console.log("authUser", _username, _token);
				_api.authUser(GAME_ID, PRIVATE_KEY, _username, _token, _onAuthUserComplete);
			}
		}
		
		private function _onAuthUserComplete(connected: Boolean): void {
			FP.console.log("_onAuthUserComplete", "connected", connected);
			_connected = connected;
			if (_connected) {
				// must retrieve:
				FP.console.log("_api.getKeyData");
				_api.getKeyData(GAME_ID, PRIVATE_KEY, "score", _retrieveScore, _username, _token);
			}
		}
		
		private function _retrieveScore(data: Object): void {
			FP.console.log("_retrieveScore", data);
			_score = parseInt(data as String);
			FP.console.log("_retrieveScore", _score);
		}
		
		public function get score():int {
			return _score;
		}
		
		public function set score(value:int):void {
			FP.console.log("set score", value);
			_api.setKeyData(GAME_ID, PRIVATE_KEY, "score", value + "", _username, _token);
			_api.setHighscore(GAME_ID, PRIVATE_KEY, value + "", value, _username, _token);
			FP.console.log("set score", "success");
			_score = value;
		}
		
		public function get username():String {
			return _username;
		}
		
		// cf http://gamejolt.com/api/doc/game/trophies/add-achieved/
		public function unlockAchievement(name:*):void {
			FP.console.log("unlockAchievement", name);
			_api.addTrophyAchieved(GAME_ID, PRIVATE_KEY, _username, _token, name as int);
		}
	}
}