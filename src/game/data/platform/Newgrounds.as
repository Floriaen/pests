package game.data.platform
{
	import com.newgrounds.API;
	import com.newgrounds.Logger;
	
	import game.data.IPlatform;
	
	import net.flashpunk.FP;
	
	public class Newgrounds implements IPlatform
	{
		private static const API_ID: String = "32390:41Rapcfo";
		private static const ENCRYPTION_KEY: String = "wGlcRburDfNronGOBLqOJEUXRptVHEt1";
		
		public static var unlocked: Object = new Object();
		public static const COLLECT_TEN_PILLS: String = "Collect 10 Pills"; // sugar
		public static const KILL_ONE_HUNDRED_ENEMIES: String = "Kill 100 Pests";
		public static const KILL_TWO_THOUSAND_ENEMIES: String = "Kill 2000 Pests";
		
		private var _saveId: String;
		
		public function Newgrounds(game: String, version: Number = 1)
		{
			if (API.connected == false) {
				API.connect(FP.stage, API_ID, ENCRYPTION_KEY, "1");
				Logger.logMessage("Newgrounds - Connected", username);
				Logger.logMessage("Version - 1");
			}
			_saveId = "Newgrounds_" + game;
		}
		
		public function get score(): int {
			Logger.logMessage("Newgrounds - Get score");
			var score: String = _getMyData("score");
			return (score == null) ? 0: parseInt(score);
		}
		
		public function set score(value: int): void {
			Logger.logMessage("Newgrounds - Save score", value);
			_setMyData("score", value + "");
			API.postScore("High score", value);
		}
		
		
		public function get username(): String {
			var name: String = API.username;
			if (name == "API-Debugger") {
				name = null;
			}
			return name;
		}
		
		public function unlockAchievement(name: *): void {
			Logger.logMessage("Unlock achievement", name);
			API.unlockMedal(name as String);
		}
		
		private function _getAllMyData(): Object {
			var data: Object = API.loadLocal(_saveId);
			Logger.logMessage(typeof(data) );
			if (data == null || typeof(data) != "object") {
				Logger.logMessage("New data created");
				data = {};
			}
			return data;
		}
		
		private function _getMyData(key: String): String {
			var data: Object = _getAllMyData();
			if (data.hasOwnProperty(key)) {
				return data[key];
			}
			return null;
		}
		
		private function _setMyData(key: String, value: String): void {
			var data: Object = _getAllMyData();
			data[key] = value;
			API.saveLocal(_saveId, data);
		}

	}
}