package game.data
{
	import com.newgrounds.Logger;
	
	import game.data.platform.AbstractPlatform;
	import game.data.platform.Gamejolt;
	import game.data.platform.Local;
	import game.data.platform.Newgrounds;
	
	import mx.utils.URLUtil;
	
	import net.flashpunk.FP;
	import net.flashpunk.utils.Data;

	public class DataManager
	{
		public static const LOCAL: int = 0;
		public static const NEWGROUNDS: int = 1;
		public static const GAMEJOLT: int = 2;
		public static const KONGREGATE: int = 3;
		
		private var _platform: IPlatform;
		
		public function DataManager(game: String, version: Number = 1.0)
		{
			//new Newgrounds(game, version);
			
			var name: String = URLUtil.getServerName(FP.stage.loaderInfo.url);
			
			if (name.indexOf("kongregate") >= 0) {
				//_platform = new Kongregate();
				
			// uploads.ungrounded.net
			} else if (name.indexOf("newgrounds") >= 0 || name.indexOf("ungrounded") >= 0) {
				_platform = new Newgrounds(game, version);
			} else if (name.indexOf("armorgames") >= 0) {
				//_platform = new Armorgames();
			} else if (name.indexOf("gamejolt") >= 0) {
				_platform = new Gamejolt();
			} else {
				_platform = new Newgrounds(game, version);
			}
			FP.console.log('DataManager', "game:", game, "version:", version, "name:", name);
			//Logger.logMessage('DataManager', "game", game, "version", version, "name", name);
		}

		public function get platform(): IPlatform {
			return _platform;
		}
	}
}