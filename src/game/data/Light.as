package game.data
{
	import flash.geom.Point;
	
	import net.flashpunk.FP;
	
	public class Light
	{
		private var _offsetX: Number = 0;
		private var _offsetY: Number = 0;
		private var _origin: Point;
		
		
		public var removed: Boolean = false;
		public var alpha: Number = 1;
		public var position: Point = new Point();		
	}
	}