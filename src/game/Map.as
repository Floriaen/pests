package game
{
	import flash.geom.Point;
	
	import game.entity.Bumper;
	import game.entity.Fire;
	import game.entity.Pill;
	import game.entity.Pipe;
	import game.entity.Portal;
	
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Tilemap;
	import net.pixelpracht.tmx.TmxObject;
	
	import sprout.flashpunk.BasicMap;
	import sprout.graphics.MyColors;
	
	public class Map extends BasicMap
	{
		private const _SOLID_TILES: Array = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42];
		
		public var pipes: Array;
		public var pillPlaces: Array;
		private var _needPill: Boolean = true;
		
		public function Map(levelClass: Class, mapClass: Class) {
			super(levelClass, "map");
			pipes = new Array();
			pillPlaces = new Array();
			loadDecorum(_SOLID_TILES, mapClass);
		}
		
		override public function added():void {
			loadObjects();
		}
		
		override protected function spawnEntity(object:TmxObject):void {
			switch(object.type) {
				case "pipe":
					var pipe: Pipe = world.create(Pipe) as Pipe;
					pipe.reset(object.x, object.y);
					pipe.aperture = object.custom["aperture"];
					if (pipe.aperture == "right") {
						(pipe.graphic as Image).flipped = true;
					}
					pipes.push(pipe);
					//trace(pipe.aperture);
					break;
				case "pill":
					pillPlaces.push(new Point(object.x, object.y));
					break;
				case "fire":
					var fire: Fire = world.create(Fire) as Fire;
					fire.reset(object.x + (Level.TILE_SIZE - fire.width) / 2, object.y + (Level.TILE_SIZE - fire.height) / 2);
					break;
				case "bumper":
					var bumper: Bumper = world.create(Bumper) as Bumper;
					if (object.custom) {
						if (object.custom.hasOwnProperty("direction")) {
							bumper.direction = object.custom["direction"];
						}
						
						if (object.custom.hasOwnProperty("jumpSize")) {
							bumper.jumpSize = parseInt(object.custom["jumpSize"]);
						}
					}
					
					bumper.reset(object.x, object.y);
					break;
				case "portal":
					var portal: Portal = world.create(Portal) as Portal;
					portal.reset(object.x, object.y);
					portal.color = MyColors[object.custom["color"]];
					break;
			}
		}
		
		override public function update():void {
			super.update();
			if (Level.gameOver) return;
			if (Level.realityIncreaseEffect > 0.3) {
				if (_needPill) {
					_needPill = false;
					var pillPlace: Point = pillPlaces[FP.rand(pillPlaces.length)];
					var pill: Pill = world.create(Pill) as Pill;
					trace('pill place 1', pillPlace.x, pillPlace.y, pill.width, pill.height);
					//pillPlace.x += (Level.TILE_SIZE - pill.width) / 2;
					//pillPlace.y += (Level.TILE_SIZE - pill.height) / 2;
				//	trace('pill place 2', pillPlace.x, pillPlace.y);
					pill.reset(pillPlace.x, pillPlace.y);
				}
				//	(graphic as Tilemap).alpha = 1 - Game.realityIncreaseEffect + 0.1;
			} else {
				(graphic as Tilemap).alpha = 1;
				_needPill = true;
			}
		}
	}
}