package game
{
	import net.flashpunk.FP;
	import net.flashpunk.World;
	
	import sprout.screen.StartScreen;
	
	public class Game extends World
	{
		public function Game()
		{
			super();
		}
		
		override public function begin(): void {
			super.begin();
			if (!FP.console.debug) {
				add(new StartScreen(FP.width, FP.height, _goToMenu, 2));
			} else {
				_goToMenu();
			}
		}
		
		private function _goToMenu(): void {
			FP.world = new Menu();
		}
	}
}