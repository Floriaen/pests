package game.fx
{
	import game.Assets;
	import game.entity.BaseEntity;
	
	import net.flashpunk.graphics.Spritemap;
	
	public class Dust extends BaseEntity
	{
		private var _sprite: Spritemap;
		
		public function Dust() {
			super();
			
			_sprite = new Spritemap(Assets.Dust, 16, 16);
			_sprite.add("current", [0, 1, 2, 3, 4, 5, 6], 12, false);
			_sprite.callback = kill;
			graphic = _sprite;
			setHitbox(16, 16);
		}
		
		override public function reset(x:Number=0, y:Number=0):void {
			super.reset(x, y);
			_sprite.play("current", true);
		}
		
		override public function update(): void {
			super.update();
			if (dead == false && _sprite.active) {
				y -= 0.1;	
			}
		}
	}
}