package game.fx
{
	import game.Assets;
	import game.Level;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.tweens.misc.NumTween;
	
	import sprout.graphics.MyColors;
	import game.entity.BaseEntity;
	
	public class Explosion extends BaseEntity
	{
		private var _scaleTween: NumTween;
		private var _scale: Number = 0;
		
		public function Explosion()
		{
			super();
			
			graphic = new Image(Assets.Explosion);
			(graphic as Image).color = MyColors.RED;
			
			type = "explosion";
			
			_scaleTween = new NumTween(kill);
			addTween(_scaleTween);
			
			scale(1);
		}
		
		protected function scale(value: Number): void {
			if (value != _scale) {
				_scale = value;
				(graphic as Image).centerOrigin();
				(graphic as Image).scale = value;
				setHitbox(20 * value, 20 * value, 10 * value, 10 * value);
			}
		}
		
		public function start(size: Number = 3, duration: Number = 1): void {
			_scaleTween.tween(1, size, duration);
			Level.sfxManager.play(Assets.ExplosionFX, 0.2);
		}
		
		override public function reset(x:Number=0, y:Number=0):void {
			_scaleTween.active = false;
			(graphic as Image).color = MyColors.RED;
			//_scale = 0;
			//scale(1);
			super.reset(x, y);
		}
		
		override public function update():void {
			super.update();
			layer -= 2;
			if (_scaleTween.active) {
				scale(_scaleTween.value);	
			} else {
				scale(1);
			}
			
			if (_scaleTween.percent < 0.5) {
				(graphic as Image).color = FP.colorLerp(MyColors.YELLOW, MyColors.RED, _scaleTween.percent * 2);	
			} else {
				(graphic as Image).color = FP.colorLerp(MyColors.RED, MyColors.BLACK, (_scaleTween.percent - 0.5)* 2 );	
			}
			
			
		}
	}
}