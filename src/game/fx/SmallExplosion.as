package game.fx
{
	import game.Assets;
	import game.entity.BaseEntity;
	
	import net.flashpunk.graphics.Spritemap;
	
	public class SmallExplosion extends BaseEntity
	{
		private var _sprite: Spritemap;
		public function SmallExplosion()
		{
			super();
			_sprite = new Spritemap(Assets.SmallExplosion, 16, 16, kill);
			_sprite.add("explode", [0, 1, 2, 3], 10);
			graphic = _sprite;
			setHitbox(16, 16);
			collidable = false;
		}
		
		override public function reset(x:Number=0, y:Number=0):void {
			super.reset(x, y);
			_sprite.play("explode");
		}
	}
}