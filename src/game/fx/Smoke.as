package game.fx
{
	import game.Assets;
	import game.entity.BaseEntity;
	
	import net.flashpunk.graphics.Spritemap;
	
	public class Smoke extends BaseEntity
	{
		private var _sprite: Spritemap;
		
		public function Smoke() {
			super();
			
			_sprite = new Spritemap(Assets.Smoke, 30, 30);
			_sprite.add("anim", [0, 1, 2, 3], 8, false);
			_sprite.callback = kill;
			graphic = _sprite;
			setHitbox(30, 30);
			layer = -5;
		}
		
		override public function reset(x:Number=0, y:Number=0):void {
			super.reset(x, y);
			_sprite.alpha = 1;
			_sprite.play("anim", true);
		}
		
		override public function update(): void {
			super.update();
			if (dead == false && _sprite.active) {
				y -= 0.1;
				_sprite.alpha -= 0.05;
			}
		}
	}
}