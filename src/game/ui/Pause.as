package game.ui
{
	import game.Assets;
	import game.Game;
	import game.Level;
	import game.entity.BaseEntity;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.utils.Input;
	
	import sprout.graphics.MyColors;
	
	public class Pause extends Entity
	{
		public function Pause(x:Number, y:Number)
		{
			super(x, y);
			
			//graphic = Image.createRect(10, 20, MyColors.RED);
			graphic = new Stamp(Assets.Pause);
			graphic.scrollX = graphic.scrollY = 0;
			setHitboxTo(graphic);
			
			layer = -100;
		}
		
		override public function update():void {
			super.update();
			if (collidePoint(x, y, world.mouseX, world.mouseY)) {
				if (Input.mousePressed) {
					Level.paused = !Level.paused; 
				}	
			}
		}
	}
}