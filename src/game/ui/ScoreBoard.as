package game.ui
{
	import flash.display.BitmapData;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.utils.Draw;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	import sprout.graphics.MyColors;
	
	public class ScoreBoard extends Entity
	{
		public function ScoreBoard(x:int, y:int, width: int, height: int)
		{
			super(x, y);
			visible = true;
			
			var bitmapData: BitmapData = new BitmapData(width, height, true, 0x00000000);
			Draw.setTarget(bitmapData);
			Draw.rectPlus(0, 0, width, height, 0, 0.7, true, 1, 12);
			graphic = new Stamp(bitmapData);
			
			
			addGraphic(new Text(
				"GAME OVER", 0, 10, 
				{
					font: "BlockedFont",
					size: 32, 
					color: MyColors.WHITE, 
					width: width, 
					wordWrap: true, 
					align: "center"
				} 
			));
			setHitbox(width, height);
			layer = -100;
		}
		
		override public function added():void {
			super.added();
			visible = true;
		}
		
		public function addScore(value: int): void {
			addGraphic(new Text(
				"Pills: " + value, 0, 48, 
				{
					size: 24, 
					//font: "MetroFont",
					color: MyColors.WHITE, 
					width: width, 
					wordWrap: true, 
					align: "center"
				} 
			));
		}
		
		public function addScoreMention(message: String): void {
			addGraphic(new Text(
				message, 0, 74, 
				{
					size: 16, 
					//font: "MetroFont",
					color: MyColors.YELLOW, 
					width: width, 
					wordWrap: true, 
					align: "center"
				} 
			));
		}
		
		public function addDetails(message: String): void {
			addGraphic(new Text(
				"Details:\n" + message, 0, 112, 
				{
					size: 12, 
					//font: "MetroFont",
					color: MyColors.WHITE, 
					width: width, 
					wordWrap: true, 
					align: "center"
				} 
			));
		}
	}
}