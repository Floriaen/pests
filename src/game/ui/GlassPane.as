package game.ui
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.tweens.misc.NumTween;
	
	import sprout.graphics.MyColors;
	
	public class GlassPane extends Entity
	{
		private var _numTween: NumTween;
		public function GlassPane()
		{
			super();
			graphic = Image.createRect(FP.width, FP.height, MyColors.PINK);
			addTween(_numTween = new NumTween(_remove));
			graphic.scrollX = graphic.scrollY = 0;
			layer = -100;
		}
		
		private function _remove(): void {
			world.remove(this);
		}
		
		public function hide(onComplete: Function = null): void {
			if (_numTween.active == false) {
				_numTween.complete = onComplete;
				_numTween.tween(1, 0, 0.9);
			}
		}
		
		override public function update():void {
			super.update();
			(graphic as Image).alpha = _numTween.value;
		}
	}
}