package game.ui
{
	import game.entity.BaseEntity;
	
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Text;
	
	import sprout.graphics.MyColors;
	
	public class Score extends Entity
	{
		private var _text: Text;
		
		public function Score(x:Number, y:Number, width: Number, height: Number)
		{
			super(x, y);
			
			_text = new Text(
				"", 0, -8, 
				{
					size: 32, 
					color: MyColors.WHITE, 
					width: width, 
					wordWrap: true, 
					align: "left"
				} 
			);
			graphic = _text;
			
			graphic.scrollX = 0;
			graphic.scrollY = 0;
			
			layer = -100;
		}
		
		public function set text(value: String): void {
			_text.text = value;
		}
	}
}