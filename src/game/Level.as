package game
{
	import com.newgrounds.Logger;
	
	import flash.geom.Point;
	
	import game.data.DataManager;
	import game.data.platform.Gamejolt;
	import game.data.platform.Newgrounds;
	import game.entity.Enemy;
	import game.entity.Energy;
	import game.entity.Pill;
	import game.entity.Pipe;
	import game.entity.Player;
	import game.entity.Small;
	import game.ui.GlassPane;
	import game.ui.Pause;
	import game.ui.Score;
	import game.ui.ScoreBoard;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Tween;
	import net.flashpunk.tweens.misc.Alarm;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	import punk.fx.effects.AdjustFX;
	import punk.fx.effects.GlitchFX;
	import punk.fx.effects.RGBDisplacementFX;
	import punk.fx.graphics.FXImage;
	
	import sprout.flashpunk.FXWord;
	import sprout.flashpunk.SFXManager;
	
	public class Level extends FXWord
	{
		public static const TILE_SIZE: uint = 16;
		public static var paused: Boolean = false;
		public static var sfxManager: SFXManager = new SFXManager();
		
		private var _level: uint = 1;
		public static var map: Map;
		public static var player: Player;
		public static var freeze: Boolean;
		
		private static const _MAX_ENEMY_COUNT: uint = 9;// 16;
		private var _enemyCount: uint = 0;
		
		private var _enemies: Array;
		private var _energies: Array;
		private var _enemyEmitionTic: Alarm;
		
		private var _enemyKilled: uint = 0;
		private var _energyCollected: uint = 0;
		private var _pillCollected: uint = 0;
		
		// ui
		private var _score: Score;
		private var _pause: Pause;
		
		public static var realityIncreaseEffect: Number = 0;  
		private var _realityIncreaseEffectAlarm: Alarm; 
		
		private var _fxScreen: FXImage;
		private var _rgbDisplacement: RGBDisplacementFX;
		private var _glitch: GlitchFX;
		private var _adjustFX: AdjustFX;
		
		private var _saturationEffectRatio: Number;
		
		private var _pillEffect: Alarm;
		
		private var _playerX: Number = 0;
		private var _playerY: Number = 0;
		
		private var _dataManager: DataManager;
		
		private var _lighting: Lighting;
		
		private var _pauseInTheLastFrame: Boolean = false;
		
		public function Level(level: uint = 1, playerX: Number = 16, playerY: Number = 16) {
			super();
			_level = level;
			_enemies = new Array();
			_energies = new Array();
			realityIncreaseEffect = 0;
			
			_rgbDisplacement = new RGBDisplacementFX([3, 2, 1, 3, 2, 1]);
			_glitch = new GlitchFX(5, 6);
			_adjustFX = new AdjustFX();
			
			_playerX = playerX;
			_playerY = playerY;
			
			Input.define("RetryKey", Key.SPACE, Key.ESCAPE, Key.ENTER);
			_dataManager = new DataManager("Pests", 1);
		}
		
		public static function get gameOver(): Boolean {
			return player && player.dead;
		}
		
		override public function end(): void {
			_enemyEmitionTic.cancel();
			removeAll();
		}
		
		public function restart(): void {
			end();
			//begin();
			FP.world = new Level(_level);
		}
		
		override public function remove(e:Entity):Entity {
			var e: Entity = super.remove(e);
			if (e is Enemy) {
				_enemyKilled++;
			}
			if (e is Small) {
				// increment the available count:
				_enemyCount -= 1;
			}
			return e;
		}
		
		override public function add(e:Entity):Entity {
			var e: Entity = super.add(e);
			if (e is Small) {
				// increment the available count:
				_enemyCount += 1;
			}
			return e;
		}
		
		private function _onEnemyEmitionTic(): void {
			var idx: uint = FP.rand(map.pipes.length);
			var pipe: Pipe = (map.pipes[idx]) as Pipe
			
			var enemyToEmit: uint = _MAX_ENEMY_COUNT - _enemyCount;
			if (enemyToEmit > 0) {
				pipe.emit(new Point(17, 17));
			}
			
			_enemies.splice(0);
			getClass(Small, _enemies);
			var enemy: Enemy = null;
			while (enemy = _enemies.shift()) {
				if (player.dead) {
					enemy.goTo(pipe.x, pipe.y); // go home
				} else {
					enemy.goTo(player.x, player.y);	
				}
			}
		}
		
		override protected function debugThings(): void {
			if (Input.pressed(Key.P)) {
				restart();
			}
			
			if (Input.pressed(Key.M)) {
				freeze = !freeze;
			}
			
			if (Input.pressed(Key.TAB)) {
				if (_level == 2) _level = 1;
				else _level += 1;
				restart();
			}
		}
		
		private function _onPillEffectEnd(): void {
			
		}
		
		private function _clearScreenEffect(): void {
			if (!player.dead) {
				_glitch.maxSlide = 0;
				_rgbDisplacement.setOffsets();
				_adjustFX.saturation = 0;
				_adjustFX.brightness = 0;
			}
		}
		
		override public function begin():void {
			//Logger.
			super.begin();
			
			add(_lighting = new Lighting());
			Lighting.lighteningEffectRatio = 0;
			
			var glassPane: GlassPane = new GlassPane();
			add(glassPane);

			addTween(_pillEffect = new Alarm(0.8, _clearScreenEffect));
			
			_fxScreen = new FXImage(FP.buffer);
			_fxScreen.effects.add([_glitch, _rgbDisplacement, _adjustFX]);
			_fxScreen.onPreRender = function(fxImage:*): void {
				FP.screen.refresh();
			};

			addGraphic(_fxScreen, -3);
			
			_glitch.maxSlide = 0;
			_rgbDisplacement.setOffsets(0, 0, 0, 0, 0, 0);
			
			
			addTween(_realityIncreaseEffectAlarm = new Alarm(0.5, _onRealityIncreaseEffect, Tween.LOOPING), true);
			
			add(_score = new Score(8, 8, FP.width, 30));
			add(_pause = new Pause(340, 8));
			
			addTween(_enemyEmitionTic = new Alarm(0.5, _onEnemyEmitionTic, Tween.LOOPING), true);
			
			// create map
			var levelClass: Class = Assets["Level" + _level];
			var mapClass: Class = Assets["Map" + _level];
			add(map = new Map(levelClass, mapClass));
			
			add(player = new Player());
			
			if (_level == 2) {
				Lighting.lighteningEffectRatio = 0.5;
				_lighting.darkness = 0.3;
				player.x = 12 * Level.TILE_SIZE;
				player.y = 8 * Level.TILE_SIZE;	
			} else {
				Lighting.lighteningEffectRatio = 0.3;
				_lighting.darkness = 0.12;
				player.x = _playerX;
				player.y = _playerY;	
			}
			
			
			_lighting.addLight(player.light);
			
			glassPane.hide(function(): void {
				if (!Level.sfxManager.isPlaying(Assets.MainTheme)) {
					Level.sfxManager.loop(Assets.MainTheme, 0.38);
				}
			});
		}
		
		private function _onRealityIncreaseEffect(): void {
			realityIncreaseEffect += 0.1;
		}
		
		override public function update():void {
			if (paused) {
				_pauseInTheLastFrame = true;
				FP.stage.frameRate = 2;
				Level.sfxManager.get(Assets.MainTheme).stop();	
				if (_pause) {
					_pause.update();
				}
				return;
			} else {
				if (_pauseInTheLastFrame == true) {
					_pauseInTheLastFrame = false;
					FP.stage.frameRate = 60;
					if (!Level.sfxManager.isPlaying(Assets.MainTheme)) {
						Level.sfxManager.get(Assets.MainTheme).resume();
					}	
				}
			}
			
			super.update();
			
			if (!Newgrounds.unlocked.hasOwnProperty(Newgrounds.COLLECT_TEN_PILLS) && _pillCollected >= 10) {
				Newgrounds.unlocked[Newgrounds.COLLECT_TEN_PILLS] = "";
				_dataManager.platform.unlockAchievement(Newgrounds.COLLECT_TEN_PILLS);
			}
			if (!Newgrounds.unlocked.hasOwnProperty(Newgrounds.KILL_ONE_HUNDRED_ENEMIES) && _enemyKilled >= 100) {
				Newgrounds.unlocked[Newgrounds.KILL_ONE_HUNDRED_ENEMIES] = "";
				_dataManager.platform.unlockAchievement(Newgrounds.KILL_ONE_HUNDRED_ENEMIES);
			}
			if (!Newgrounds.unlocked.hasOwnProperty(Newgrounds.KILL_TWO_THOUSAND_ENEMIES) && _enemyKilled >= 2000) {
				Newgrounds.unlocked[Newgrounds.KILL_TWO_THOUSAND_ENEMIES] = "";
				_dataManager.platform.unlockAchievement(Newgrounds.KILL_TWO_THOUSAND_ENEMIES);
			}
			
			
			if (!Gamejolt.unlocked.hasOwnProperty(Gamejolt.DUMMY_TROPHY) && _pillCollected >= 10) {
				Gamejolt.unlocked[Gamejolt.DUMMY_TROPHY] = "";
				_dataManager.platform.unlockAchievement(Gamejolt.DUMMY_TROPHY);
			}
			if (!Gamejolt.unlocked.hasOwnProperty(Gamejolt.ASSASIN) && _enemyKilled >= 100) {
				Gamejolt.unlocked[Gamejolt.ASSASIN] = "";
				_dataManager.platform.unlockAchievement(Gamejolt.ASSASIN);
			}
			if (!Gamejolt.unlocked.hasOwnProperty(Gamejolt.GENOCIDE) && _enemyKilled >= 2000) {
				Gamejolt.unlocked[Gamejolt.GENOCIDE] = "";
				_dataManager.platform.unlockAchievement(Gamejolt.GENOCIDE);
			}
			
			Level.freeze = (player.dead) || _pillEffect.active;
			if (_pillEffect.active) {
				var p: Number = 1 - _pillEffect.percent;
				_rgbDisplacement.setOffsets(4 * p, 3 * p, 2 * p, 4 * p, 3 * p, 2 * p);	
				_glitch.maxSlide = p * 4;
				_adjustFX.saturation += _pillEffect.percent * _saturationEffectRatio;
			}
			
			_score.text = _pillCollected + "";
			if (player.dead) {
				if (map.active == true) {
					realityIncreaseEffect = 0;
					_saturationEffectRatio = 1 / (1 - _adjustFX.saturation);
					_pillEffect.start();
					_enemies.splice(0);
					getType("enemy", _enemies);
					while (enemy = _enemies.shift()) {
						enemy.kill();
					}
					
					_enemyEmitionTic.cancel();
					map.active = false;
					
					
					// display the scoreBoard:
					var sbWidth: Number = 240;
					var sbHeight: Number = 120;
					var sbX: Number = (FP.width - sbWidth) / 2;
					var sbY: Number = (FP.height - sbHeight) / 2;
					
					var scoreBoard: ScoreBoard = new ScoreBoard(sbX, sbY, sbWidth, sbHeight);
					scoreBoard.addScore(_pillCollected);
					
					var bestScore: int = _dataManager.platform.score;
					Logger.logMessage("BEST SCORE " + bestScore);
					if (bestScore < _pillCollected) {
						_dataManager.platform.score = _pillCollected;
						scoreBoard.addScoreMention("New high score!");
					} else if (bestScore == _pillCollected) {
						scoreBoard.addScoreMention("1 more?");
					} else {
						var diff: int = (bestScore - _pillCollected);
						if (diff == 1) {
							scoreBoard.addScoreMention(diff + " pill more to your high score!");	
						} else {
							scoreBoard.addScoreMention(diff + " pills more to your high score!");
						}
					}
					
					add(scoreBoard);
					
					
					
				} else {
					if (Input.pressed("RetryKey")) {
						restart();	
					}
				}
				
				return;
			}
			
			if (player.collidable) {
				var energy: Energy = null;
				_energies.splice(0);
				player.collideInto("energy", player.x, player.y, _energies);
				if (_energies.length > 0) {
					while (energy = _energies.shift()) {
						energy.kill();
						_energyCollected++;
					}
				}
				
				var enemy: Enemy = player.collide("enemy", player.x, player.y) as Enemy;
				if (enemy && enemy.dead == false) {
					player.kill();
				}
				
				var pill: Pill = player.collide("pill", player.x, player.y) as Pill; 
				if (pill) {
					Level.sfxManager.play(Assets.HornFX);
					
					pill.kill();
					_pillCollected++;
					
					realityIncreaseEffect = 0;
					_saturationEffectRatio = 1 / (1 - _adjustFX.saturation);
					_pillEffect.start();
					
					_enemies.splice(0);
					getClass(Small, _enemies);
					while (enemy = _enemies.shift()) {
						if (FP.random > 0.8) 
							enemy.kill();
					}
				}
			}
		}
	}
}