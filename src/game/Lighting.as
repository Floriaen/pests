package game
{
	import flash.display.BitmapData;
	import flash.display.BlendMode;
	import flash.geom.ColorTransform;
	
	import game.data.Light;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	
	public class Lighting extends Entity
	{
		public static var lighteningEffectRatio: Number = 0.3;
		
		private var _pixels: BitmapData;
		private var _pixelsWithAlpha: BitmapData;
		
		private var _colorTransform: ColorTransform = new ColorTransform(1, 1, 1, 0.12);
		private var _light: Image = new Image(Assets.Light);
		private var _lights: Array;
		private var _noLights: Boolean = false;
		
		public function Lighting() {
			super();
			_light.centerOO();
			_pixels = new BitmapData(FP.width, FP.height, false, 0xFFFFFF);
			_pixelsWithAlpha = new BitmapData(FP.width, FP.height, true, 0x000000);
			
			_lights = new Array();
			
			layer = -13;
		}
		
		public function set darkness(value: Number): void {
			_colorTransform = new ColorTransform(1, 1, 1, value);
		}
		
		public function turnOffTheLights(): void {
			_noLights = true; // show all
		}
		
		public function isEntityInTheDarkness(entity: Entity, threshold: uint = 60): Boolean {
			FP.rect.x = entity.x;
			FP.rect.y = entity.y;
			FP.rect.width = entity.width;
			FP.rect.height = entity.height;
			var result: Boolean = !_pixelsWithAlpha.hitTest(FP.camera, threshold, FP.rect);
			return result;
		}
		
		public function addLight(light: Light): void {
			_lights.push(light);
		}
		
		override public function render():void {
			
			FP.rect.setEmpty();
			FP.rect.width = FP.width;
			FP.rect.height = FP.height;
			
			//redraw the canvas
			_pixels.fillRect(FP.rect, 0xFFFFFFFF);
			_pixelsWithAlpha.fillRect(FP.rect, 0x00000000);
			if (_noLights == false) {
				for (var i: uint = 0; i < _lights.length; i++) {
					var l: Light = _lights[i];
					if (l.removed) {
						_lights.splice(i, 1);
					} else {
						if (l.alpha > 0) {
							if (lighteningEffectRatio > 0) {
								_light.scale = 1 + FP.random * lighteningEffectRatio;
							}
							_light.alpha = l.alpha;
							_light.render(_pixels, l.position, FP.camera);

							if (lighteningEffectRatio > 0) {
								_light.scale = 1.1 - lighteningEffectRatio;	
							}
							_light.render(_pixelsWithAlpha, l.position, FP.camera);	
						}
					}
				}	
			}
			FP.buffer.draw(_pixels, null, _colorTransform, BlendMode.SUBTRACT, null, false);
			super.render();
		}
	}
}