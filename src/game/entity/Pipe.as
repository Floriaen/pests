package game.entity
{
	import game.Assets;
	
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	
	import sprout.graphics.MyColors;
	
	public class Pipe extends BaseEntity
	{
		
		public var aperture: String = "left";
		
		public function Pipe()
		{
			super();
			//graphic = Image.createRect(2 * 16, 2 * 16, MyColors.GREEN);
			graphic = new Image(Assets.Pipe);
			setHitboxTo(graphic);
			type = "pipe";
			dynamicLayer = false;
			layer = -3;
		}
		
		public function emit(target: *): void {
			//return;
			var enemy: Enemy = world.create(Small) as Enemy;
			var ex: Number = 0;
			var ey: Number = 0;
			switch (aperture) {
				case "left":
					ex = x - enemy.width;
					ey = y + (height - enemy.height) / 2;		
					break;
				case "right":
					ex = x + width + enemy.width;
					ey = y + (height - enemy.height) / 2;
					break;
			}
			
			enemy.reset(ex, ey);
			enemy.goTo(target.x, target.y);
		}
	}
}