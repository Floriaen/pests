package game.entity
{
	import flash.geom.Point;
	
	import game.Assets;
	import game.Level;
	
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.tweens.misc.Alarm;
	
	import sprout.graphics.MyColors;
	import game.fx.Dust;
	
	public class Gun extends BaseEntity
	{
		private var _armed: Boolean = true;
		
		private var _alarm: Alarm;
		
		public function Gun()
		{
			super();
			graphic = Image.createRect(4, 4, MyColors.RED);
			collidable = false;
			addTween(_alarm = new Alarm(0.30, arm));
		}
		
		public function arm(): void {
			_armed = true;
		}
		
		public function fire(velocity: Point): void {
			if (_armed) {
				_armed = false;
				_alarm.start();
				var bullet: Bullet = world.create(Bullet) as Bullet;
				bullet.reset(x, y);
				bullet.velocity.x = velocity.x;
				bullet.velocity.y = velocity.y;
				
				var dust: Dust = world.create(Dust) as Dust;
				if (velocity.x > 0) {
					dust.reset(x, y - dust.halfHeight);
				} else if (velocity.x < 0) {
					dust.reset(x - dust.width, y - dust.halfHeight);
				} else if (velocity.y > 0) {
					dust.reset(x - dust.halfWidth, y);
				} else {
					dust.reset(x - dust.halfWidth, y - dust.height);
				}
				Level.sfxManager.play(Assets.GunFireFX, 0.2, true);
			}
		}
	}
}