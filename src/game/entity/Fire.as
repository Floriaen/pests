package game.entity
{
	import game.Assets;
	import game.data.Light;
	
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Spritemap;
	
	public class Fire extends BaseEntity
	{
		public var light: Light;
		private var _sprite: Spritemap;
		
		public function Fire() {
			super();
			_sprite = new Spritemap(Assets.Fire, 8, 8);
			_sprite.add("burning", [0, 1, 2, 3], 4);
			
			graphic = _sprite;
			
			_sprite.play("burning");
			_sprite.index = FP.rand(3);
			
			setHitbox(8, 8);
			type = "fire";
			light = new Light();
			light.position.x = centerX;
			light.position.y = centerY;
			layer = 0;
		}
		
		override public function update():void {
			super.update();
			light.position.x = centerX;
			light.position.y = centerY;
		}
	}
}