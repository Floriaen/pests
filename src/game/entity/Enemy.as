package game.entity
{
	import flash.geom.Point;
	
	import game.Level;
	import game.Map;
	
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.tweens.misc.Alarm;
	import net.flashpunk.tweens.misc.VarTween;
	import net.flashpunk.tweens.motion.LinearPath;
	import net.flashpunk.tweens.motion.Motion;
	
	import sprout.flashpunk.Utils;
	import sprout.flashpunk.tweens.motion.DynamicLinearPath;
	import sprout.graphics.MyColors;
	
	public class Enemy extends Follower
	{
		protected var life: int = 0;		
		private var _colorChangeAlarm: Alarm;
		
		public function Enemy()
		{
			super();
			type = "enemy";
			life = 1;
			layer = -2;
			
			addTween(_colorChangeAlarm = new Alarm(0.4, _onColorChanged));
			
			//solidTypes.push("bomb");
		}
		
		override public function reset(x:Number=0, y:Number=0):void {
			life = 1;
			super.reset(x, y);
		}
		
		public function hurt(): void {
			
			if (--life < 0) life == 0;
			if (life > 0 && _colorChangeAlarm.active == false) {
				(graphic as Image).tinting = 0.8;
				(graphic as Image).tintMode = Image.TINTING_COLORIZE;
				(graphic as Image).color = MyColors.RED;//MyColors.RED;//(0 & 1) ? 0xff0000 : 0xffff80;
				_colorChangeAlarm.start();
				
				//velocity.x
				//velocity.y
			}
		}
		
		private function _onColorChanged(): void {
			(graphic as Image).tintMode = Image.TINTING_MULTIPLY;
			(graphic as Image).color = 0x00FFFFFF;
		}
		
		override public function kill():void {
			life = 0;
			var energy: Energy = world.create(Energy) as Energy;
			energy.reset(x + (width - energy.width) / 2, y + (height - energy.height) / 2);
			super.kill();
		}
		
		override public function update():void {
			super.update();
			if (!dead && life == 0) kill();
			
			(graphic as Image).flipped = (velocity.x > 0);
			
			var bullet: Bullet = collide("bullet", x, y) as Bullet;
			if (bullet) {
				//velocity.x += bullet.velocity.x;
				//velocity.y += bullet.velocity.y;
				hurt();
			}
			
			if (collide("explosion", x, y)) {
				kill();
			}
		}
	}
}