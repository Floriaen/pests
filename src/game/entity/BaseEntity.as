package game.entity
{
	import flash.display.BitmapData;
	import flash.geom.Point;
	
	import game.Level;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.masks.Pixelmask;
	import net.flashpunk.tweens.misc.NumTween;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.tweens.motion.Motion;
	
	public class BaseEntity extends Entity
	{
		public static const NEARLY_0:Number = 0.01;
		
		public var dead: Boolean = false;

		public var velocity: Point;
		public var friction: Number = 1;
		
		private var _entities: Array;
		protected var solidTypes: Array;
		public var canJump: Boolean = false;
		
		
		private var _jumpMotion: LinearMotion;
		
		protected var bypassMovement: Boolean = false;
		protected var dynamicLayer: Boolean = true;
		
		public function BaseEntity()
		{
			super();

			
			velocity = new Point();
			
			_entities = new Array();
			solidTypes = new Array();
			solidTypes.push("map");
			
			addTween(_jumpMotion = new LinearMotion(onJumpMotionEnd));
		}
		
		protected function setPixelMaskTo(source: Graphic): void {
			var bitmapData: BitmapData = new BitmapData(16, 16, true, 0);
			source.render(bitmapData, FP.zero, FP.zero);
			mask = new Pixelmask(bitmapData);
		}
		
		protected function onJumpMotionEnd(): void {
			scaleForJump(1);
		}
		
		public function get moving(): Boolean {
			return (velocity.x != 0 || velocity.y != 0);
		}
		
		public function jumpTo(toX: Number, toY: Number): void {
			trace("jump to", toX, toY);
			if (canJump && _jumpMotion.active == false) {
				_jumpMotion.setMotion(x, y, toX, toY, 0.4);
			}
		}
		
		public function scaleForJump(value: Number): void {
			if (canJump == false) return;
			var image: Image = (graphic as Image);
			if (image) {
				if (value != image.scale) {
					collidable = (Math.abs(value) <= 1);
					if (value != 1) {
						image.x = - (image.scaledWidth - image.width) / 2;
						image.y = - (image.scaledHeight - image.height) / 2 + ((1 - value) * 10);
					} else {
						image.x = 0;
						image.y = 0;
					}
					
					image.scale = value;
				}
			}
		}
		
		public function kill(): void {
			dead = true;
			world.recycle(this);
		}
		
		public function reset(x: Number = 0, y: Number = 0): void {
			_jumpMotion.active = false;
			scaleForJump(1);
			velocity.x = velocity.y = 0;
			dead = false;
			active = true;
			
			this.x = x;
			this.y = y;
		}
		
		override public function toString():String {
			return '{x: ' + x + ', y: ' + y + ', w: ' + width + ', h: ' + height + '}';
		}
		
		override public function update(): void {
			super.update();
			if (dead == true) return;
			
			if (dynamicLayer) {
				var tileY:int = Math.floor((y) / Level.TILE_SIZE);
				layer = -tileY;
			}
			
			
			if (_jumpMotion.active) {
				layer -= 1;
				moveTo(_jumpMotion.x, _jumpMotion.y);
				var p: Number = _jumpMotion.percent;
				var z: Number = 2 - 4 * (p - 0.5) * (p - 0.5);
				scaleForJump(z);
				return;
			}
			
			velocity.x *= friction;
			velocity.y *= friction;
			
			if (Math.abs(velocity.x) < NEARLY_0) velocity.x = 0;
			if (Math.abs(velocity.y) < NEARLY_0) velocity.y = 0;
			
			if (bypassMovement == false)
				moveBy(velocity.x, velocity.y, solidTypes);
		}
	}
}