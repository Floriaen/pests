package game.entity
{
	import flash.display.Sprite;
	
	import game.Assets;
	import game.Level;
	import game.fx.SmallExplosion;
	import game.fx.Smoke;
	
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.masks.Pixelmask;
	
	import sprout.graphics.MyColors;

	public class Small extends Enemy
	{
		private var _sprite: Spritemap;
		private var _target: BaseEntity;
		private var _pills: Array;
		
		public function Small()
		{
			super();
			//graphic = new Image(Assets.Small);//Image.createRect(10, 10, MyColors.MAROON); 
			_sprite = new Spritemap(Assets.Small, 10, 12);
			_sprite.add("walking", [0, 1, 2, 1], 4);
			_sprite.play("walking");
			graphic = _sprite;

			setHitboxTo(graphic);
			//setPixelMaskTo(graphic);
			
			life = 1;
			speed = FP.rand(4) * 0.1 + 0.6;// 1;//0.8;
			
			_pills = new Array();
		}
		
		override public function reset(x:Number=0, y:Number=0):void {
			super.reset(x, y);
			speed = FP.rand(4) * 0.1 + 0.6;
			life = 1;
		}
		
		override public function kill():void {
			var explosion: BaseEntity = world.create(SmallExplosion) as BaseEntity;
			explosion.reset(x + (width - explosion.width) / 2, y + (height - explosion.height) / 2);
			super.kill();
		}
		
		override public function update():void {
			super.update();
			if (dead) return;
			var energy: Energy = collide("energy", x, y) as Energy;
			if (energy && energy.dead == false) {
				energy.kill();
				
				var smoke: Smoke = world.create(Smoke) as Smoke;
				smoke.reset(x + (width - smoke.width) / 2, y + (height - smoke.height) / 2);
				
				// change to big
				var big: Big = world.create(Big) as Big;
				big.reset(x + (width - big.width) / 2, y + (height - big.height) / 2);
				kill();
			}
		}
	}
}