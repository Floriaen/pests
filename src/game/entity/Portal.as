package game.entity
{
	import game.Assets;
	import game.Game;
	import game.Level;
	
	import net.flashpunk.FP;
	import net.flashpunk.Tween;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.tweens.misc.VarTween;
	
	import sprout.graphics.MyColors;

	public class Portal extends BaseEntity
	{
		private var _portals: Array;
		internal var walkThroughBy: BaseEntity;
		private var _rotateAnimation: VarTween;
		
		public function Portal()
		{
			super();
			//graphic = Image.createCircle(8, MyColors.WHITE);
			graphic = new Image(Assets.Portal);
			(graphic as Image).centerOrigin();
			
			
			addTween(_rotateAnimation = new VarTween(null, Tween.LOOPING));
			_rotateAnimation.tween((graphic as Image), "angle", 360, 4);
			
			setHitbox(8, 8, -4, -4);
			type = "portal";
			_portals = new Array();	
			dynamicLayer = false;
		}
		
		override public function reset(x: Number = 0, y: Number = 0): void {
			super.reset(x + width, y + height);
			centerOrigin();
		}
		
		public function set color(value: uint): void {
			(graphic as Image).color = value;
		}
		
		override public function update(): void {
			super.update();
			
			if (walkThroughBy) {
				if (collideWith(walkThroughBy,  x - halfWidth, y - halfHeight)) {
					return;
				} else {
					walkThroughBy = null;
				}
			}
			var entity: BaseEntity = collideTypes(["player", "bomb", "enemy"], x - halfWidth, y - halfHeight) as BaseEntity;
			if (entity) {
				// get the other portal
				_portals.splice(0);
				world.getClass(Portal, _portals);
				var portal: Portal = null;
				while (portal = _portals.shift()) {
					if (portal != this) {
						portal.walkThroughBy = entity;
						
						var toX: Number = portal.x + (entity.x - x);
						var toY: Number = portal.y + (entity.y - y);
						entity.moveTo(toX, toY);
						
						var follower: Follower = entity as Follower;
						if (follower) {
							follower.clearPath();
							follower.goTo(Level.player.x, Level.player.y);
						}
						
						break;
					}
				}
				
			}	
		}
	}
}