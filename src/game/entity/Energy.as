package game.entity
{
	import game.Assets;
	
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	
	import sprout.graphics.MyColors;
	
	public class Energy extends BaseEntity
	{
		public function Energy()
		{
			super();
			graphic = new Image(Assets.Energy);//Image.createCircle(3, MyColors.WHITE);
			setHitboxTo(graphic);
			type = "energy";
		}
	}
}