package game.entity
{
	import flash.display.BitmapData;
	import flash.geom.Point;
	
	import flashx.textLayout.debug.assert;
	
	import game.Assets;
	import game.Level;
	import game.Map;
	import game.data.Light;
	import game.fx.Explosion;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Canvas;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.masks.Grid;
	import net.flashpunk.masks.Pixelmask;
	import net.flashpunk.tweens.misc.Alarm;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.tweens.motion.Motion;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	import sprout.graphics.MyColors;
	
	public class Player extends BaseEntity
	{
		protected var speed: Number = 2;
		protected var linearMotion: LinearMotion;
		protected var to: Point;
		//protected var moving: Boolean = false;

		public var gun: Gun;
		public var bomb: Bomb;
		
		private var _gunForce: Number = 0.33;
		
		public var isLeft: Boolean = false;
		public var isRight: Boolean = false;
		public var isUp: Boolean = false;
		public var isDown: Boolean = false;
		
		private var _sprite: Spritemap;
		
		private var _dustAlarm: Alarm;
		
		public var light: Light;
		
		
		public function Player()
		{
			super();
			//bypassMovement = true;
			//graphic = Image.createRect(16, 16, MyColors.BLUE);
			//graphic = new Image(Assets.Player);
			_sprite = new Spritemap(Assets.Player, 16, 16);
			_sprite.add("walking", [0, 1, 2, 1], 6);
			graphic = _sprite;
			
			setHitboxTo(graphic);
			//setPixelMaskTo(graphic);
			
			velocity = new Point();
			addTween(linearMotion = new LinearMotion(_onMotionComplete));
			
			to = new Point();
			
			gun = new Gun();
			
			Input.define("MoveUp", Key.Z, Key.W);
			Input.define("MoveDown", Key.S);
			Input.define("MoveRight", Key.D);
			Input.define("MoveLeft", Key.Q, Key.A);
			
			
			Input.define("FireUp", [Key.UP]);
			Input.define("FireDown", [Key.DOWN]);
			Input.define("FireLeft", [Key.LEFT]);
			Input.define("FireRight", [Key.RIGHT]);
			
			Input.define("Bomb", [Key.E]);
			
			type = "player";
			
			layer = -3;
			
			canJump = true;
			
			addTween(_dustAlarm = new Alarm(0.4, _onDustAlarmReached));
			light = new Light();
		}
		
		private function _onDustAlarmReached(): void {
			//var dust: Dust = world.create(Dust) as Dust;
			//dust.reset(x, y);
		}
		
		override public function kill():void {
			if (dead) return;
			super.kill();
			light.removed = true;
			var explosion: Explosion = world.create(Explosion) as Explosion;
			
			explosion.reset(x - (width - explosion.width) / 2, y - (height - explosion.height) / 2);
			explosion.start(1.1, 0.4);
			trace('player', this);
			trace('explosion', explosion, (explosion.graphic as Image).scale);
			gun.kill();
		}
		
		override public function added():void {
			super.added();
			world.add(gun);
			light.removed = false;
			light.position.x = x;
			light.position.y = y;
		}
		
		private function _onMotionComplete(): void {
		//	moving = false;
		}
		
		override public function update(): void {
			super.update();
			layer -= 1;
			
			light.position.x = x;
			light.position.y = y;
			
			if (collide("explosion", x, y)) {
				kill();
			}

			gun.visible = Input.check("fire");
			
			velocity.x = velocity.y = 0;
			if (Input.check("MoveUp")) {
				velocity.y = -speed;
				
				gun.x = x;
				gun.y = y - gun.height;
				
				isLeft = false;
				isRight = false;
				isUp = true;
				isDown = false;
				
			} else if (Input.check("MoveDown")) {
				velocity.y = speed;
				gun.x = x;
				gun.y = y + height;
				
				isLeft = false;
				isRight = false;
				isUp = false;
				isDown = true;
			}
			
			if (Input.check("MoveRight")) {
				velocity.x = speed;
				gun.x = x + width;
				gun.y = y;
				
				isLeft = false;
				isRight = true;
				isUp = false;
				isDown = false;
				
				
			} else if (Input.check("MoveLeft")) {
				velocity.x = -speed;
				gun.x = x - gun.width;
				gun.y = y;
				
				isLeft = true;
				isRight = false;
				isUp = false;
				isDown = false;
			}
			
			FP.point.x = FP.point.y = 0;
			if (Input.check("FireUp")) {
				FP.point.y = -5;
				gun.x = x + (width - gun.width) / 2;
				gun.y = y - 1;
				gun.fire(FP.point);
				velocity.y += _gunForce;
			} else if (Input.check("FireDown")) {
				FP.point.y = 5;
				gun.x = x + (width - gun.width) / 2;
				gun.y = y + height + 1;
				gun.fire(FP.point);
				velocity.y -= _gunForce;
			} else if (Input.check("FireLeft")) {
				FP.point.x = -5;
				gun.x = x - 1;
				gun.y = y + (height - gun.height) / 2;
				gun.fire(FP.point);
				(graphic as Image).flipped = true;
				velocity.x += _gunForce;
			} else if (Input.check("FireRight")) {
				FP.point.x = 5;
				gun.x = x + width + 1;
				gun.y = y + (height - gun.height) / 2;
				gun.fire(FP.point);
				(graphic as Image).flipped = false;
				velocity.x -= _gunForce;
			}
			
			if (Input.pressed("Bomb")) {
				if (bomb == null || bomb.dead == true) {
					bomb = world.create(Bomb) as Bomb;
					bomb.reset(x + 2, y + 2);
				}
			}
			
			if (velocity.x != 0 || velocity.y !=0) {
				_sprite.play("walking");
				if (_dustAlarm.active == false) {
					_dustAlarm.reset(0.4);
				}
			} else {
				_dustAlarm.active = false;
				_sprite.setFrame(0);
			}
		}
		
		// http://truk2uf.googlecode.com/svn-history/r49/trunk/bomberman_jpo/clients/flash/src/bomberman/Game.as
		override public function moveCollideX(e:Entity):Boolean {
			if (e is Map) {
				//if (velocity.x != 0) return true;
				var posYInTile: Number = (y % 16) / 16;
				var column: int = Math.floor(x / 16);
				var row: int = Math.floor(y / 16);
				var xSign: int = FP.sign(velocity.x);
				
				trace("column:", column, "row:", row, "position in column:", posYInTile, "y:", y);
				// horizontal collision check: test the top block
				var isSolid: Boolean = ((e as Map).mask as Grid).getTile(column + xSign, row + 1);
				if (!isSolid) {
					trace("row", row + 1, "is not solid");
					if (posYInTile >= 0.1) {
						trace('move y from', y, 'to', (row + 1) * 16);
						y += 1;
					}
				} else {
					// test the bottom block
					isSolid = ((e as Map).mask as Grid).getTile(column + xSign, row);
					if (!isSolid) {
						trace("row", row, "is not solid");
						if (posYInTile <= 0.9) {
							trace('move y from', y, 'to', row * 16);
							y -= 1;
						}
					}
				}
				
			}
			return true;
		}
		
		override public function moveCollideY(e:Entity):Boolean {
			if (e is Map) {
				//if (velocity.y != 0) return true;
				var ySign: int = FP.sign(velocity.y);
				
				var posXInTile: Number = (x % 16) / 16;
				var column: int = Math.floor(x / 16);
				var row: int = Math.floor(y / 16);
				trace("column:", column, "row:", row, "position in row:", posXInTile, "x:", x);
				// vertical collision check: test the right block
				var isSolid: Boolean = ((e as Map).mask as Grid).getTile(column + 1, row + ySign);
				if (!isSolid) {
					trace("column", column + 1, "is not solid");
					if (posXInTile >= 0.1) {
						trace('move x from', x, 'to', (column + 1) * 16);
						x += 1;						
					}
				} else {
					// test the left block
					isSolid = ((e as Map).mask as Grid).getTile(column, row + ySign);
					if (!isSolid) {
						trace("column", column, "is not solid");
						if (posXInTile <= 0.9) {
							trace('move x from', x, 'to', column * 16);
							x -= 1;							
						}
					}
				}				
			}
			return true;
		}
	}
}