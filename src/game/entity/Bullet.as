package game.entity
{
	import flash.geom.Point;
	
	import game.Assets;
	
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	
	import sprout.graphics.MyColors;
	
	public class Bullet extends BaseEntity
	{
		
		public function Bullet()
		{
			super();
			//graphic = Image.createCircle(2, MyColors.RED);
			graphic = new Image(Assets.Bullet);
			setHitboxTo(graphic);
			type = "bullet";
			layer = -3;
		}
		
		override public function reset(x:Number=0, y:Number=0):void {
			super.reset(x, y);
		}
		
		override public function update():void {
			super.update();
			moveTo(x + velocity.x, y + velocity.y);
			if (collideTypes(['map', 'enemy', 'bomb'], x, y)) {
				kill();
			}
		}

		
	}
}