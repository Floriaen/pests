package game.entity
{
	import game.Assets;
	
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	
	import sprout.graphics.MyColors;
	
	public class Pill extends BaseEntity
	{
		public function Pill()
		{
			super();
			graphic = new Image(Assets.Pill);
			// 16 / 16
			setHitbox(16, 16);//, -2, -3);
			(graphic as Image).x = 2;
			(graphic as Image).y = 3;
			//setHitboxTo(graphic);
			setPixelMaskTo(graphic);
			type = "pill";
			layer = -1;
		}
									  
	}
}