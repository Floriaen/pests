package game.entity
{
	import game.Assets;
	import game.Game;
	import game.Level;
	
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	
	import sprout.graphics.MyColors;

	public class Bumper extends BaseEntity
	{
		public var direction: String = "both";
		public var jumpSize: uint = 2;
		public function Bumper()
		{
			super();
			//graphic = Image.createRect(16, 16, MyColors.BLUE);
			graphic = new Image(Assets.Bumper);
			setHitboxTo(graphic);
			type = "bumper";
			dynamicLayer = false;
		}
		
		override public function update(): void {
			super.update();
			var entity: BaseEntity = collideTypes(["player", "bomb"], x, y) as BaseEntity;
			if (entity) {
				var xSign: int = 0;
				var ySign: int = 0;
				if (direction == "horizontal") {
					xSign = FP.sign(entity.velocity.x);
				} else if (direction == "vertical") {
					ySign = FP.sign(entity.velocity.y);
				} else {
					// both
					xSign = FP.sign(entity.velocity.x);
					ySign = FP.sign(entity.velocity.y);
				}
				
				trace(entity.velocity.x, entity.velocity.y);
				entity.jumpTo(x + xSign * jumpSize * Level.TILE_SIZE, y + ySign * jumpSize * Level.TILE_SIZE);
			}			
		}
	}
}