package game.entity
{
	import flash.geom.Point;
	
	import game.Game;
	import game.Level;
	
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.masks.Hitbox;

	public class Follower extends BaseEntity
	{
		private var _path: Array;
		
		private var _current: Point;
		private var _distance: Number;
		private var _step: Number;
		
		protected var speed: Number = 1;
		
		public function Follower()
		{
			super();
			_path = new Array();
			_current = null;
		}
		
		public function get pathLength(): uint {
			return (_path == null) ? 0: _path.length;
		}
		
		
		override public function reset(x:Number=0, y:Number=0):void {
			super.reset(x, y);
			_path.splice(0);
			_current = null;
		}
		
		public function clearPath(): void {
			_path.splice(0);
			_current = null;
		}
		
		public function goTo(x: Number, y: Number): void {
			// clear
			_path.splice(0);
			
			FP.point.x = x;
			FP.point.y = y;
			var path: Array = Level.map.findPath(this, FP.point);
			if (path) {
				var w: int = (graphic as Image).width
				var h: int = (graphic as Image).height
				var decX: Number = (Level.TILE_SIZE - w) / 2;
				var decY: Number = (Level.TILE_SIZE - h) / 2;
				trace(decX, decY);
				while (path.length > 0) {
					_path.push(new Point(path.shift() * 16 + decX, path.shift() * 16 + decY));
				}
			}
		}
		
		override public function update():void {
			super.update();
			if (Level.freeze) {
				velocity.x = velocity.y = 0;
				return;
			}
			if (_current == null) {
				if (_path && _path.length > 0) {					
					_current = _path.shift();
				}
			} else {
				// check position:
				var distance: Number = FP.distance(x, y, _current.x, _current.y);
				if (distance < 1) {
					_current = _path.shift();
				}
			}
			
			if (_current != null) {
				var diffX: Number = x - _current.x;
				var diffY: Number = y - _current.y;
				
				var newX: Number = x;
				var newY: Number = y;
				
				if (diffX < 0) {
					newX = Math.min(_current.x, x + speed);
				} else if (diffX > 0) {
					newX = Math.max(_current.x, x - speed);
				}
				
				if (diffY < 0) {
					newY = Math.min(_current.y, y + speed);
				} else if (diffY > 0) {
					newY = Math.max(_current.y, y - speed);
				}
				
				velocity.x = newX - x;
				velocity.y = newY - y;
			}
		}
	}
}