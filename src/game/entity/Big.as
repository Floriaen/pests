package game.entity
{
	import game.Assets;
	import game.Game;
	import game.Level;
	
	import net.flashpunk.FP;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.masks.Pixelmask;
	
	import sprout.graphics.MyColors;
	import game.fx.Explosion;

	public class Big extends Enemy
	{
		
		private var _target: BaseEntity;
		private var _energies: Array;
		
		private var _sprite: Spritemap;
		
		public function Big()
		{
			super();
			//graphic = new Image(Assets.Big);//Image.createRect(16, 16, MyColors.PURPLE);
			_sprite = new Spritemap(Assets.Big, 16, 16);
			_sprite.add("walking", [0, 1, 2, 1], 4);
			_sprite.play("walking");
			graphic = _sprite;
			
			setHitboxTo(graphic);
			//setPixelMaskTo(graphic);
			
			speed = 0.8;//0.6;
			_energies = new Array();
			
			life = 2;
		}
		
		override public function kill():void {
			var explosion: Explosion = world.create(Explosion) as Explosion;
			explosion.reset(x - (width - explosion.width) / 2, y - (height - explosion.height) / 2);
			explosion.start(1.6, 0.4);
			super.kill();
		}
		
		override public function reset(x:Number=0, y:Number=0):void {
			super.reset(x, y);
			life = 2;
		}
		
		override public function update():void {
			super.update();
			
			if (_target && _target.dead || pathLength == 0) {
				_target = null;
			}
			// if no target or target is not an energy
			if (_target == null || !(_target is Energy)) {
				_energies.splice(0);
				world.getClass(Energy, _energies);
				if (_energies.length > 0) {
					_target = _energies[FP.rand(_energies.length)];
					goTo(_target.x, _target.y);
				} else {
					// follow the player if there is no energy available
					_target = Level.player;
					goTo(_target.x, _target.y);	
				}
			}
			
			var energy: Energy = collide("energy", x, y) as Energy;
			if (energy) {
				energy.kill();
			}
		}
	}
}