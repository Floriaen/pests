package game.entity
{
	import game.Assets;
	import game.Level;
	
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.masks.Pixelmask;
	import net.flashpunk.tweens.misc.Alarm;
	
	import sprout.flashpunk.FXWord;
	import sprout.graphics.MyColors;
	import game.fx.Explosion;
	
	public class Bomb extends BaseEntity
	{
		
		private var _countDown: Alarm;
		private var _life: int = 2;
		
		public function Bomb()
		{
			super();
			//graphic = Image.createCircle(8, MyColors.BLACK);
			graphic = new Image(Assets.Bomb);
			setHitboxTo(graphic);
			
			type = "bomb";
			
			addTween(_countDown = new Alarm(2, kill), true);	
			canJump = true;
			
			//layer = -1;
			
			friction = 0.8;
			dynamicLayer = false;
		}
		
		override protected function onJumpMotionEnd(): void {
			super.onJumpMotionEnd();
			if (dead == false) kill();
		}
		
		override public function reset(x:Number=0, y:Number=0):void {
			_countDown.reset(2);
			_life = 2;
			super.reset(x, y);
		}
		
		override public function kill():void {
			trace("bomb explosion", x, y);
			//Level.sfxManager.play(Assets.Explosion);
			_countDown.active = false; // in case it still running
			var explosion: Explosion = world.create(Explosion) as Explosion;
			trace(explosion);
			explosion.reset(x - (width - explosion.width) / 2, y - (height - explosion.height) / 2); 
			explosion.start();
			(world as FXWord).shake(20, 1.4);
			super.kill();
		}
		
		override public function update(): void {
			layer = 0;
			super.update();
			var bullet: Bullet = collide("bullet", x, y) as Bullet;
			if (bullet) {
				_life--;
				if (_life < 0) _life = 0;
				if (_life == 0) {
					kill();
				} else {
					velocity.x = bullet.velocity.x;
					velocity.y = bullet.velocity.y;
					trace(velocity.x, velocity.y);
				}
				
			} else if (collide("sword", x, y)) {
				//kill();
			} else if (collide("enemy", x, y)) {
				//kill();
			}
		}
	}
}