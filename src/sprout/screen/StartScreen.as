package sprout.screen
{	
	import flash.display.BitmapData;
	
	import net.flashpunk.Engine;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Canvas;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.tweens.misc.Alarm;
	import net.flashpunk.tweens.misc.NumTween;
	
	import sprout.graphics.MyColors;
	
	public class StartScreen extends Entity
	{
	
		[Embed(source = '04B_24.ttf', embedAsCFF="false", fontFamily = '_pixelFont')]
		private static const _pixelFont:Class;
		
		[Embed(source="skill.png")]
		private var _skill: Class;
		
		private var _fade: NumTween;
		private var _onRemoved: Function;
		
		public function StartScreen(width:uint, height:uint, onRemoved: Function, zoom: Number = 1) {
			visible = false;
			_onRemoved = onRemoved;

			var skillImage: Image = new Image(_skill);
			skillImage.scale = zoom;
			skillImage.centerOO();
			skillImage.x = width / 2;
			skillImage.y = height / 2;
			
			var screen: Canvas = new Canvas(width, height);
			//background.color = MyColors.GRAY;
			FP.rect.setEmpty();
			FP.rect.width = width;
			FP.rect.height = height;
			screen.fill(FP.rect, MyColors.BLACK);
			
			
			screen.drawGraphic(0, 0, skillImage);
			
			var text: Text = new Text(
				"@floriaen", 0, 100, 
				{
					font: "_pixelFont",
					size: 8 * zoom, 
					color: 0xFFFFFF, 
					width: FP.width, 
					wordWrap: true, 
					align: "center"
				}
			);
			
			//text.centerOrigin();
			screen.drawGraphic(0, height - 168, text);
			
			graphic = screen;
			
			_fade = new NumTween(_waitAndRemoved);
			addTween(_fade);
		}
		
		private function _waitAndRemoved(): void {
			var alarm: Alarm = new Alarm(0.3, removed);
			addTween(alarm, true);
		}
		
		override public function removed():void {
			super.removed();
			if (_onRemoved != null) _onRemoved();
			world.remove(this);
		}
		
		override public function added(): void {
			super.added();
			visible = true;
			_fade.tween(0, 1, 2);
		}
		
		override public function update():void {
			super.update();
			if (_fade.active) {
				(graphic as Canvas).alpha = _fade.value;
			}
		}
	}
}