package sprout.graphics
{
	import net.flashpunk.FP;

	public class MyColors
	{
		//http://en.wikipedia.org/wiki/List_of_colors:_A-M
		
		public static var WHITE: uint = 0xFFFFFFFF;
		public static var GRAY: uint = 0xFF808080;
		public static var SLIVER: uint = 0xFFC0C0C0;
		public static var BLACK: uint = 0xFF000000;
		public static var MAROON: uint = 0xFF800000;
		public static var OLIVE: uint = 0xFF808000
		public static var RED: uint = 0xFFFF0000;
		public static var BLUE: uint = 0xFF0000FF;
		public static var GREEN: uint = 0xFF008000;
		public static var LIME: uint = 0xFF00FF00;
		public static var TEAL: uint = 0xFF008080;
		public static var AQUA: uint = 0xFF00FFFF;
		public static var NAVY: uint = 0xFF000080;
		public static var FUCHSIA: uint = 0xFFFF00FF;
		public static var PINK: uint = 0xFFFFCBDB;
		public static var PURPLE: uint = 0xFF800080;
		public static var YELLOW: uint = 0xFFFFFF00;
		public static var ORANGE: uint = 0xFFED7F10;
		
		
		public static function randomMix(color1: uint, color2: uint, color3: uint, greyControl: Number): uint {
			var randomIndex: uint = Math.random() * 3;
			
			var mixRatio1: Number = (randomIndex == 0) ? Math.random() * greyControl : Math.random();
			var mixRatio2: Number = (randomIndex == 1) ? Math.random() * greyControl : Math.random();
			var mixRatio3: Number = (randomIndex == 2) ? Math.random() * greyControl : Math.random();
			
			var sum: Number = mixRatio1 + mixRatio2 + mixRatio3;
			
			mixRatio1 /= sum;
			mixRatio2 /= sum;
			mixRatio3 /= sum;
			
			var rgba1: Array = MyColors.getRGBA(color1);
			var rgba2: Array = MyColors.getRGBA(color2);
			var rgba3: Array = MyColors.getRGBA(color3);
			
			return MyColors.makeColor(
				(mixRatio1 * rgba1[0] + mixRatio2 * rgba2[0] + mixRatio3 * rgba3[0]),
				(mixRatio1 * rgba1[1] + mixRatio2 * rgba2[1] + mixRatio3 * rgba3[1]),
				(mixRatio1 * rgba1[2] + mixRatio2 * rgba2[2] + mixRatio3 * rgba3[2]), 0
			);
		}
		
		public static function getRGBA(Color:uint,Results:Array=null):Array {
			if(Results == null)
				Results = new Array();
			Results[0] = (Color >> 16) & 0xFF;
			Results[1] = (Color >> 8) & 0xFF;
			Results[2] = Color & 0xFF;
			Results[3] = Number((Color >> 24) & 0xFF) / 255;
			return Results;
		}
		
		public static function makeColor(Red:uint, Green:uint, Blue:uint, Alpha:Number=1.0):uint {
			return (((Alpha>1)?Alpha:(Alpha * 255)) & 0xFF) << 24 | (Red & 0xFF) << 16 | (Green & 0xFF) << 8 | (Blue & 0xFF);
		}
		
		public static function interpolateColors(color1:uint, color2:uint, ratio:Number):uint {
			var inv:Number = 1 - ratio;
			var red:uint = Math.round( ( ( color1 >>> 16 ) & 255 ) * ratio + ( ( color2 >>> 16 ) & 255 ) * inv );
			var green:uint = Math.round( ( ( color1 >>> 8 ) & 255 ) * ratio + ( ( color2 >>> 8 ) & 255 ) * inv );
			var blue:uint = Math.round( ( ( color1 ) & 255 ) * ratio + ( ( color2 ) & 255 ) * inv );
			var alpha:uint = Math.round( ( ( color1 >>> 24 ) & 255 ) * ratio + ( ( color2 >>> 24 ) & 255 ) * inv );
			return ( alpha << 24 ) | ( red << 16 ) | ( green << 8 ) | blue;
		}
	}
}