package sprout.graphics
{
	import flash.display.BitmapData;
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.filters.BlurFilter;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import net.flashpunk.FP;

	public class BitmapDataHelper
	{
		private static var _point: Point = new Point();
		private static var _rectangle: Rectangle = new Rectangle();
		private static var _blurFilter: BlurFilter = new BlurFilter();
		private static var _matrix: Matrix = new Matrix();
		private static var _sprite: Sprite = new Sprite;
		private static var _graphic: Graphics = _sprite.graphics;
		
		// Used for rad-to-deg and deg-to-rad conversion.
		/** @private */ public static const DEG:Number = -180 / Math.PI;
		/** @private */ public static const RAD:Number = Math.PI / -180;
		
		
		public static function createAlphaMask(fillWithcolor: uint, bitmapData: BitmapData): void {
			for (var i: uint= 0; i < bitmapData.width; i++) {
				for (var j: uint = 0; j < bitmapData.height; j++) {
					var c: uint = bitmapData.getPixel32(i, j);
					// compare alpha
					if ((c>>24)&0xFF > 0) {
						bitmapData.setPixel32(i, j, fillWithcolor);
					}
				}
			}
		}
		
		public static function drawGradient(bitmapData: BitmapData, colorA:uint = 0xFFFFFF, colorB:uint = 0x000000, angle:Number = 90): void {
			_matrix.identity();
			_matrix.createGradientBox(bitmapData.width, bitmapData.height, angle * (-RAD));
			_graphic.clear();
			_graphic.beginGradientFill(GradientType.LINEAR, [colorA, colorB], [1, 0.7], [0, 255], _matrix);
			_graphic.drawRect(0, 0, bitmapData.width, bitmapData.width);
			bitmapData.draw(_sprite);
		}
		
		public static function createShadow(bitmapData: BitmapData, blurX: Number = 0, blurY: Number = 0): BitmapData {
			BitmapDataHelper._point.x = BitmapDataHelper._point.y = 0;
			BitmapDataHelper._rectangle.setEmpty();
			BitmapDataHelper._rectangle.width = bitmapData.width;
			BitmapDataHelper._rectangle.height = bitmapData.height;
			
			BitmapDataHelper._blurFilter.blurX = blurX;
			BitmapDataHelper._blurFilter.blurY = blurY;
			var bitmapDataOut: BitmapData = bitmapData.clone();//new BitmapData(bitmapData.width, bitmapData.height, true, 0x00000000);
			BitmapDataHelper.createAlphaMask(0xFF000000, bitmapDataOut);
			bitmapDataOut.applyFilter(bitmapDataOut, BitmapDataHelper._rectangle, BitmapDataHelper._point, BitmapDataHelper._blurFilter);
			return bitmapDataOut;
		}
	}
}