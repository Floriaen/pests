package sprout.flashpunk
{
	import net.flashpunk.Sfx;

	public class SFXManager
	{
		private static var _sfxCache: Object;
		
		public function SFXManager()
		{
			_sfxCache = {};
		}
		
		public function stop(soundClass: Class): void {
			if (_sfxCache.hasOwnProperty(soundClass)) {
				(_sfxCache[soundClass] as Sfx).stop();
			}
		}
		
		public function isPlaying(soundClass: Class): Boolean {
			if (_sfxCache.hasOwnProperty(soundClass)) {
				var sfx: Sfx = _sfxCache[soundClass];
				return sfx.playing;
			}
			return false;
		}
		
		public function loop(soundClass: Class, volume: Number = 0.6, noCache: Boolean = false): void {
			get(soundClass, noCache).loop(volume);
		}
		
		public function play(soundClass: Class, volume: Number = 0.6, noCache: Boolean = false): void {
			get(soundClass, noCache).play(volume);
		}
		
		public function get(soundClass: Class, noCache: Boolean = false): Sfx {
			var sfx: Sfx = null;
			if (noCache) {
				sfx = new Sfx(soundClass);
			} else {
				if (!_sfxCache.hasOwnProperty(soundClass)) {
					_sfxCache[soundClass] = new Sfx(soundClass);
				} 
				sfx = _sfxCache[soundClass];
			}
			return sfx;
		}
	}
}