package sprout.flashpunk
{
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.LoaderInfo;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.ProgressEvent;
	import flash.text.Font;
	import flash.text.TextField;
	import flash.utils.getDefinitionByName;
	
	
	public class Preloader extends MovieClip 
	{
		private var square:Sprite = new Sprite();
		private var border:Sprite = new Sprite();
		private var size:Number = 256;
		//private var text:TextField = new TextField();
		
		private var txtColor:uint = 0xFFFFFF;
		private var loaderColor:uint = 0xFFFFFF;
		
		private var begun:Boolean = false;
		private var _mainClass: String = null;
		
		public function Preloader(mainClass: String) 
		{
			
			//stop();
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			_mainClass = mainClass;
			
			addEventListener(Event.ENTER_FRAME, checkFrame);
			loaderInfo.addEventListener(ProgressEvent.PROGRESS, progress);
			// show loader
			addChild(square);
			
			square.x = (stage.stageWidth / 2) -((size-8)/2);
			square.y = stage.stageHeight / 2;
			
			addChild(border);
			border.x = (stage.stageWidth / 2) - (size/2);
			border.y = stage.stageHeight / 2 - 4;
			
			//addChild(text);
			//text.x = (stage.stageWidth / 2) - (size/2);
			//text.y = stage.stageHeight / 2 - 30;
			
		}
		
		private function progress(e:ProgressEvent):void 
		{
			if (!begun) {
				begun = true;
			}
			trace(loaderInfo.bytesLoaded, loaderInfo.bytesTotal);
			// update loader
			square.graphics.beginFill(loaderColor);
			square.graphics.drawRect(0,0, (loaderInfo.bytesLoaded / loaderInfo.bytesTotal) * (size-6), 20);
			square.graphics.endFill();
			
			//text.textColor = txtColor;
			//text.text = "Loading: " + Math.ceil((loaderInfo.bytesLoaded/loaderInfo.bytesTotal)*100) + "%";
			
		}
		
		private function checkFrame(e:Event):void 
		{
			//if we're done, run the startup function, remove event listener
			if (currentFrame == totalFrames) 
			{
				removeEventListener(Event.ENTER_FRAME, checkFrame);
				loaderInfo.removeEventListener(ProgressEvent.PROGRESS, progress);
				startup();
			}
		}
		
		private function startup():void 
		{
			
			// hide loader
			// stop();
			//nextFrame();
			
			//remove all the children
			var i:int = numChildren;
			while (i --) removeChildAt(i)
			
			//go to the main class
			//loaderInfo.removeEventListener(ProgressEvent.PROGRESS, progress);
			var mainClass:Class = getDefinitionByName(_mainClass) as Class;
			addChild(new mainClass as DisplayObject);
			
		}
		
	}
	
}