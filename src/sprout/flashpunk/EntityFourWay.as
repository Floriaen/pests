package sprout.flashpunk
{
	import flash.geom.Point;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	
	/**
	 * ...
	 * @author Dreauw
	 */
	public class EntityFourWay extends Entity {
		
		public var velocity : Point = new Point(0, 0);
		public var maxVelocity : Point = new Point(90, 800);
		public var acceleration : Point = new Point(0, 0);
		public var friction : Number = 0.3;
		public var gravity : Number = 1300;
		public var onGround : Boolean = false;
		public var solidTypes: Array = new Array();
		public var dead: Boolean = false;
		
		public function EntityFourWay() {
			super();
		}
		
		public function isDead(): Boolean {
			return dead;
		}
		
		override public function removed():void {
			super.removed();
			dead = true;
			graphic = null;
		}
		
		public function notMove() : void {
			velocity.x = 0;
			velocity.y = 0;
			acceleration.x = 0;
			acceleration.y = 0;
		}
		
		public function moveLeft(nbr:Number) : void {
			if (acceleration.x > 0) acceleration.x = 0;
			acceleration.x -= nbr;
		}
		
		public function moveRight(nbr:Number) : void {
			if (acceleration.x < 0) acceleration.x = 0;
			acceleration.x += nbr;
		}
		
		public function moveUp(nbr:Number) : void {
			if (acceleration.y > 0) acceleration.y = 0;
			acceleration.y += -nbr;
		}
		
		public function moveDown(nbr:Number) : void {
			if (acceleration.y < 0) acceleration.y = 0;
			acceleration.y += nbr;
		}
		
		override public function update():void {
			super.update();
			//if (dead) return;
			
			velocity.x += acceleration.x * FP.elapsed;
			velocity.x *= friction;
			
			velocity.y += acceleration.y * FP.elapsed;
			velocity.y *= friction;
			
		//	if (Math.abs(velocity.y) > Math.abs(maxVelocity.y)) velocity.y = FP.sign(velocity.y) * maxVelocity.y;
		//	if (Math.abs(velocity.x) > Math.abs(maxVelocity.x)) velocity.x = FP.sign(velocity.x) * maxVelocity.x;
			
			onGround = false;
			//trace(velocity);
			moveBy(velocity.x * FP.elapsed, velocity.y * FP.elapsed, solidTypes);
		}
		
		override public function moveCollideX(e:Entity):Boolean {
			velocity.x = 0;
			acceleration.x = 0;
			return super.moveCollideX(e);
		}
		
		override public function moveCollideY(e:Entity):Boolean {
			velocity.y = 0;
			acceleration.y = 0;
			return super.moveCollideY(e);
		}
	}
	
}