package sprout.flashpunk
{
	import net.flashpunk.FP;

	public class Utils
	{
		public static function get randomSign(): int {
			return (FP.random > 0.5) ? 1: -1;
		}
	}
}