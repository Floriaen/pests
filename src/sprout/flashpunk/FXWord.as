package sprout.flashpunk
{
	import flash.text.engine.FontPosture;
	
	import net.flashpunk.FP;
	import net.flashpunk.World;
	import net.flashpunk.tweens.misc.VarTween;
	
	// from http://pastebin.com/qHbYJQ7J
	public class FXWord extends World
	{
		public var shakeTime:int = 0;
		public var shakeAmount:Number = 0;
		public var time:Number = 0;
		
		public var scale: Number = 1;
		private var _scaleTween: VarTween;
		
		public function FXWord()
		{
			super();
		}
		
		protected function debugThings(): void {
			
		}
		
		override public function update(): void {
			debugThings();
			
			super.update();
			if (_scaleTween && _scaleTween.active) {
				//trace(scale);
				FP.engine.getChildAt(0).scaleX = scale; 
				FP.engine.getChildAt(0).scaleY = scale;
			}
		}
		
		override public function render(): void {
			if (shakeTime > 0) {
				shakeTime -= FP.elapsed;
				var x:Number = camera.x;
				var y:Number = camera.y;
				FP.camera.x = Math.round((camera.x - shakeAmount) + shakeAmount * 2 * FP.random);
				FP.camera.y = Math.round((camera.y - shakeAmount) + shakeAmount * 2 * FP.random);
				super.render();
				camera.x = x;
				camera.y = y;
			} else {
				super.render();
				shakeTime = 0;
			}
		}
		
		/*
		public function rotateTo(value: Number, duration: Number = 2): void {
		
		}
		*/
		public function scaleTo(value: Number, duration: Number = 2): void {
			if (_scaleTween == null) {
				addTween(_scaleTween = new VarTween());
			}
			_scaleTween.tween(this, "scale", value, duration);
		}
		
		public function shake(time:int = 20, amount:Number = 4):void {
			shakeTime = time;
			shakeAmount = amount;
		}
	}
}