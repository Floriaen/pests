package sprout.flashpunk
{
	import flash.geom.Point;
	
	import net.flashpunk.Entity;
	import net.flashpunk.Graphic;
	import net.flashpunk.Mask;
	import net.flashpunk.graphics.TiledImage;
	import net.flashpunk.graphics.Tilemap;
	import net.flashpunk.masks.Grid;
	import net.pixelpracht.tmx.TmxMap;
	import net.pixelpracht.tmx.TmxObject;
	import net.pixelpracht.tmx.TmxObjectGroup;
	
	import org.policyalmanac.AStarLib;
	
	public class BasicMap extends Entity
	{
		internal const DECORUM_LAYER: String = "decorum";
		internal const OBJECTS_LAYER: String = "objects";
		internal const TILESET_NAME: String = "tileset";
		
		public var tileMap: Tilemap;
		protected var aStarLib: AStarLib;
		protected var tmxMap: TmxMap;
		
		public function BasicMap(levelClass: Class, typeName: String = "map")
		{
			super();
			
			var xml: XML = new XML((new levelClass()).toString());
			tmxMap = new TmxMap(xml);
			
			aStarLib = new AStarLib(tmxMap.width, tmxMap.height, tmxMap.tileWidth);
			aStarLib.InitializePathfinder();

			type = typeName;
		}
		
		public function findPath(source: *, target: *): Array {
			if (!aStarLib) return null;
			var pathFound: int = aStarLib.FindPath(0, source.x, source.y, target.x, target.y);
			if (pathFound == AStarLib.FOUND) {
				//trace(aStarLib.pathBank);
				return aStarLib.pathBank[0];
			}
			return null;
		}
		
		protected function loadDecorum(solid: Array, tileResource: Class): void {
			var mapWidth: Number = tmxMap.width * tmxMap.tileWidth;
			var mapHeight: Number = tmxMap.height * tmxMap.tileHeight;
			var mapCsv: String = tmxMap.getLayer(DECORUM_LAYER).toCsv(tmxMap.getTileSet(TILESET_NAME));
			tileMap = new Tilemap(tileResource, mapWidth, mapHeight, tmxMap.tileWidth, tmxMap.tileHeight);
			tileMap.loadFromString(mapCsv);
			
			var grid: Grid = new Grid(mapWidth, mapHeight, tmxMap.tileWidth, tmxMap.tileHeight);
			var line: String = "";
			for (var j: uint = 0; j < tileMap.rows; j++) {
				line = "";
				for (var i: uint = 0; i < tileMap.columns; i++) {
					var idx: uint = tileMap.getTile(i, j);
					line += idx;
					
					var isSolid: Boolean = (solid.indexOf(idx) >= 0);
					grid.setTile(i, j, isSolid);
					
					// a star
					if (!aStarLib.walkability[i]) {
						aStarLib.walkability[i] = [];
					}
					aStarLib.walkability[i][j] = isSolid;
				}
				trace(line);
			}
			
			mask = grid;
			graphic = tileMap;
		}
		
		protected function loadObjects(): void {
			var group: TmxObjectGroup = tmxMap.getObjectGroup(OBJECTS_LAYER);
			for each(var object: TmxObject in group.objects) {
				spawnEntity(object);
			}
		}
		
		protected function spawnEntity(object: TmxObject): void {
			
		}
		
		override public function removed():void {
			super.removed();
			aStarLib.EndPathfinder();
			aStarLib = null;
		}
	}
}