package sprout.flashpunk
{
	import flash.geom.Point;
	
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	
	/**
	 * ...
	 * @author Dreauw
	 */
	public class EntityBase extends Entity {
		protected var velocity : Point = new Point(0, 0);
		protected var maxVelocity : Point = new Point(90, 800);
		public var acceleration : Point = new Point(0, 0);
		protected var friction : Number = 0.3;
		protected var gravity : Number = 1300;
		protected var onGround : Boolean = false;
		protected var solidTypes: Array = new Array();
		protected var dead: Boolean = false;

		
		public function EntityBase() {
			super();
			layer = 1;
		}
		
		public function isDead(): Boolean {
			return dead;
		}
		
		override public function removed():void {
			super.removed();
			dead = true;
			graphic = null;
		}
		
		public function notMove() : void {
			acceleration.x = 0;
		}
		
		public function moveLeft(nbr:Number) : void {
			if (acceleration.x > 0) acceleration.x = 0;
			acceleration.x -= nbr;
		}
		
		public function moveRight(nbr:Number) : void {
			if (acceleration.x < 0) acceleration.x = 0;
			acceleration.x += nbr;
		}
		
		public function jump(nbr:Number) : void {
			onGround = false;
			velocity.y = -nbr;
		}
		
		override public function update():void {
			super.update();
			//if (dead) return;
			
			velocity.x += acceleration.x * FP.elapsed;
			velocity.x *= friction;
			
			velocity.y += acceleration.y * FP.elapsed;
			
			if (Math.abs(velocity.y) > Math.abs(maxVelocity.y)) velocity.y = FP.sign(velocity.y) * maxVelocity.y;
			if (Math.abs(velocity.x) > Math.abs(maxVelocity.x)) velocity.x = FP.sign(velocity.x) * maxVelocity.x;
			
			onGround = false;
			//trace(velocity);
			moveBy(velocity.x * FP.elapsed, velocity.y * FP.elapsed, solidTypes, true);
		}
		
		override public function moveCollideX(e:Entity):Boolean {
			velocity.x = 0;
			acceleration.x = 0;
			return super.moveCollideX(e);
		}
		
		override public function moveCollideY(e:Entity):Boolean {
			// Ceiling
			if (velocity.y < 0) {
				velocity.y = 0;
			} else {
				//velocity.y = 0;
				acceleration.y = gravity;
				if (e.y > y + height - 2) {
					onGround = true;	
				}
				
			}
			return super.moveCollideY(e);
		}
	}
	
}