package sprout.core
{
	public class ObjectHelper
	{
		public static function merge(object1: Object, object2: Object): Object {
			for (var prop: String in object2) {
				if (!object1[prop]) {
					object1[prop] = object2[prop];
				}
			}
			return object1;
		}
		
		public static function remove(list: Array, idx: uint): Array {
			var l: uint = list.length;
			var out: Array = new Array();
			for(var i:int = 0; i < l; i++) {
				if (i != idx) out.push(list[i]);
			}
			return out;
		}
	}
}