package sprout.core
{
	import com.adobe.serialization.json.JSON;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;

	public class RestHelper
	{
		public function RestHelper()
		{
		}
		
		public static function call(url: String, method:String, params:Array, onSuccess:Function = null, onError: Function = null):void {
			var variables:URLVariables = new URLVariables();
			variables.method = method;
			variables.input_type = "JSON"
			variables.response_type = "JSON";
			variables.rest_data = JSON.decode(params);
			
			var request:URLRequest = new URLRequest(url);
			request.method = URLRequestMethod.POST;
			request.data = variables;
			
			var urlLoader:URLLoader = new URLLoader();
			urlLoader.dataFormat = URLLoaderDataFormat.TEXT;
			if (onSuccess)
				urlLoader.addEventListener(Event.COMPLETE, onSuccess);
			if (onError) 
				urlLoader.addEventListener(IOErrorEvent.IO_ERROR, onError);
			urlLoader.load(request);
		}
		/*
		private function onDataLoaded(event:Event):void {
			var urlLoader:URLLoader = event.target as URLLoader;
			
			var resultData:Object = JSON.decode(urlLoader.data as String);
			trace("[data loaded] result_count:", resultData.result_count);
			urlLoader.removeEventListener(Event.COMPLETE, onDataLoaded);
		}
		*/
		/*
		private function urlLoaderTestIoError(event:IOErrorEvent):void {
			trace("urlLoaderTestIoError: " + event);
		}
		*/
	}
}