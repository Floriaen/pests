package 
{
	import game.Game;
	import game.Menu;
	
	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Key;
	
	import sprout.graphics.MyColors;
	
	//[SWF(width="768", height="512", backgroundColor="#000000")]
	[SWF(width="883", height="614", backgroundColor="#000000")]
	[Frame(factoryClass="Preloader")]
	public class Pests extends Engine
	{
		public function Pests() {
			super(368, 256);//384, 256);
			FP.screen.scale = 2.4;
			FP.screen.color = MyColors.BLACK;
			//FP.console.enable();
			//FP.console.toggleKey = Key.U;
			//FP.console.log(this);
			FP.world = new Game();
		}
		
		override public function update(): void {
			super.update();
		}
	}
}